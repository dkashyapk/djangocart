from django.contrib import admin
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('products/<int:id>', views.productsView),
    path('AboutUs/', views.aboutus),
    path('ContactUs/', views.contactus),
    path('login/', views.login),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
