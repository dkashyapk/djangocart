from django.shortcuts import render
from math import ceil
from shop.models import Product, Contact


def index(request):
    allProds = []
    catprods = Product.objects.values('category')
    cats = {item['category'] for item in catprods}
    for cat in cats:
        prod = Product.objects.filter(category=cat)
        n = len(prod)
        nSlides = n // 4 + ceil((n / 4) - (n // 4))
        allProds.append([prod, range(1, nSlides), nSlides])
    param = {'allProds': allProds}

    return render(request, 'index.html', param)


def productsView(request, id):
    product = Product.objects.filter(id=id)
    return render(request, 'productsView.html', {'product':product[0]})


def aboutus(request):
    return render(request, 'aboutus.html')


def login(request):
    return render(request, 'login.html')


def contactus(request):
    if request.method == 'POST':
        name = request.POST.get('name','')
        email = request.POST.get('email','')
        phone = request.POST.get('phone','')
        desc = request.POST.get('desc','')
        contact = Contact(name=name,desc=desc,phone=phone,email=email)
        contact.save()
    return render(request, 'contactus.html')